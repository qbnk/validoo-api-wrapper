### Scraping GS1 code tables ###
Code tables are available at https://gs1.se/en/guides/documentation/code-lists/

This seemed to work to scrape any of the code lists pages. Uncomment the "if sweden" whenever you want to scrape 
only swedish/all values (when that code list has the "only in sweden" column):
```
let codes = {}; 
jQuery('table.table-striped.responsive').find('td[data-th="Code value"]').map(function() {
const cell = jQuery(this),
name = cell.next(),
sweden = cell.next().next().next();
//if (sweden.find('span').text() === 'X') {
codes[cell.find('span').text().toLowerCase()] = name.find('span').text();
//}
});
JSON.stringify(codes);
```
