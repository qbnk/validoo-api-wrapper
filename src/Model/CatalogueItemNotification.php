<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model;

use Adbar\Dot;
use QBNK\Integration\Validoo\Model\Enum\CinFormat;
use QBNK\Integration\Validoo\Model\Enum\UnitDescriptorCode;

/**
 * Catalogue Item Notification
 * A CIN is a carrier message that carries information about one trade item hierarchy. It also contains
 * information about the content owners at the different levels (Message, Transaction and Document
 * command), plus the Information provider and Data recipient and other relevant data about the trade item.
 * The json response from the two GET endpoints includes some trade item attributes but the details lie
 * encoded in the attribute called “cin”. This attribute is a base64 string and decoded it contains all the trade
 * item information in the requested format. XML is default but text can be requested. If the item searched for
 * has any children, they are included in the “cin”.
 */
class CatalogueItemNotification extends Dot
{
    private const VALUE_BOOLEAN = 1;
    private const VALUE_DATETIME = 2;
    private const VALUE_INTEGER = 5;
    private const VALUE_STRING = 6;

    public const VALUE_NOT_AVAILABLE = 'n/a';

    protected string $unitDescriptorCode;

    private static ?array $measurementCodes;

    public function __construct(array $items = [], string $unitDescriptorCode = UnitDescriptorCode::BASE_UNIT_OR_EACH)
    {
        $this->unitDescriptorCode = $unitDescriptorCode;
        parent::__construct($items);
    }

    /**
     * @throws ValidooApiException
     */
    public static function fromPayload(string $cinData, string $format = CinFormat::XML): CatalogueItemNotification
    {
        if ($format !== CinFormat::XML) {
            throw new ValidooApiException('Can only decode XML cin data');
        }

        $doc = new \DOMDocument();
        if (!@$doc->loadXML(base64_decode($cinData))) {
            throw new ValidooApiException('Could not decode cin data');
        }

        $data = [];

        foreach ($doc->getElementsByTagName('tradeItem')->item(0)->childNodes as $tradeItemField) {
            /** @var \DOMElement $tradeItemField */
            switch ($tradeItemField->nodeName) {
                case 'gtin':
                    $data['gtin'] = $tradeItemField->nodeValue;
                    break;

                case 'tradeItemUnitDescriptorCode':
                    $unitDescriptorCode = $tradeItemField->nodeValue;
                    break;

                case 'nextLowerLevelTradeItemInformation':
                    $data['containerInfo'] = [];
                    foreach ($tradeItemField->childNodes as $childNode) {
                        /** @var \DOMElement $childNode */
                        switch ($childNode->nodeName) {
                            case 'quantityOfChildren':
                            case 'totalQuantityOfNextLowerLevelTradeItem':
                                $data['containerInfo'][$childNode->nodeName] = $childNode->nodeValue;
                        }
                    }
                    break;

                case 'tradeItemInformation':
                    self::extractTradeItemInformation($tradeItemField->firstChild, $data);
            }
        }

        return new CatalogueItemNotification($data, $unitDescriptorCode ?? UnitDescriptorCode::BASE_UNIT_OR_EACH);
    }

    /**
     * @throws ValidooApiException
     */
    private static function extractTradeItemInformation(\DOMElement $tradeItemInformation, array &$data): void
    {
        $containmentLevels = ['CONTAINS' => 'Contains', 'FREE_FROM' => 'Free from', 'MAY_CONTAIN' => 'May contain'];

        foreach ($tradeItemInformation->childNodes as $section) {
            /** @var \DOMElement $section */
            $nodeName = $section->nodeName;
            try {
                switch ($nodeName) {
                    case 'delivery_purchasing_information:deliveryPurchasingInformationModule':
                    case 'duty_fee_tax_information:dutyFeeTaxInformationModule':
                    case 'trade_item_data_carrier_and_identification:tradeItemDataCarrierAndIdentificationModule':
                    case 'trade_item_size:tradeItemSizeModule':
                    case 'variable_trade_item_information:variableTradeItemInformationModule':
                        break;

                    case 'trade_item_description:tradeItemDescriptionModule':
                        self::extractLanguageVariants($section, 'descriptionShort', $data);
                        break;

                    case 'allergen_information:allergenInformationModule':
                        $allergenCodes = json_decode(file_get_contents(__DIR__ . '/../gs1codes/allergencodes.json'), true);
                        $data['allergens'] = [];
                        foreach ($section->getElementsByTagName('allergen') as $allergen) {
                            /** @var \DOMElement $allergen */
                            $levelOfContainment = $allergen->getElementsByTagName('levelOfContainmentCode')->item(0)->nodeValue;
                            if (array_key_exists($levelOfContainment, $containmentLevels)) {
                                $levelOfContainment = $containmentLevels[$levelOfContainment] . ': ';
                            } else {
                                //Some containment codes aren't expected to be encountered here, parenthesise them just in case
                                $levelOfContainment = '('. $levelOfContainment .') ';
                            }

                            $allergens = [];
                            foreach ($allergen->getElementsByTagName('allergenTypeCode') as $allergenCode) {
                                /** @var \DOMElement $allergenCode */
                                $allergenCodeKey = $allergenCodes[strtolower($allergenCode->nodeValue)];
                                $allergens[] = array_key_exists('sv', $allergenCodeKey) ? $allergenCodeKey['sv'] : $allergenCodeKey['en'];
                            }
                            $data['allergens'][] = $levelOfContainment . implode(', ', $allergens);
                        }
                        break;

                    case 'food_and_beverage_ingredient:foodAndBeverageIngredientModule':
                        self::extractLanguageVariants($section, 'ingredientStatement', $data);
                        break;
                    case 'marketing_information:marketingInformationModule':
                        self::extractLanguageVariants($section, 'tradeItemMarketingMessage', $data);
                        break;
                    case 'consumer_instructions:consumerInstructionsModule':
                        self::extractLanguageVariants($section, 'consumerUsageInstructions', $data);
                        break;

                    case 'nutritional_information:nutritionalInformationModule':
                        $nutrientCodes = json_decode(file_get_contents(__DIR__ . '/../gs1codes/nutrientcodes_sv.json'), true);
                        $data['nutritional'] = [];

                        foreach ($section->getElementsByTagName('nutrientDetail') as $nutrientDetail) {
                            /** @var \DOMElement $nutrientDetail */
                            $nutrient = lcfirst(str_replace(' ', '', ucwords($nutrientCodes[strtolower($nutrientDetail->firstChild->nodeValue)])));
                            $data['nutritional'][$nutrient] = [];
                            foreach ($nutrientDetail->getElementsByTagName('quantityContained') as $quantity) {
                                /** @var \DOMElement $quantity */
                                $data['nutritional'][$nutrient][] = $quantity->nodeValue . ' '
                                    . self::getMeasurement($quantity->getAttribute('measurementUnitCode'));
                            }
                        }
                        break;

                    case 'packaging_information:packagingInformationModule':
                        $packagingCodes = json_decode(file_get_contents(__DIR__ . '/../gs1codes/packagecodes.json'), true);
                        $data['packaging'] = [];
                        foreach ($section->getElementsByTagName('packaging') as $packagingNode) {
                            /** @var \DOMElement $packagingNode */
                            $packaging = [];
                            foreach ($packagingNode->childNodes as $childNode) {
                                /** @var \DOMElement $childNode */
                                if ($childNode->nodeName === 'packagingTypeCode') {
                                    $packaging['packagingType'] = $packagingCodes[strtolower($childNode->nodeValue)];
                                } elseif ($childNode->nodeName === 'packagingWeight') {
                                    $packaging['packagingWeight'] = $childNode->nodeValue . ' '
                                        . self::getMeasurement($childNode->getAttribute('measurementUnitCode'));
                                }
                            }
                            $data['packaging'][] = $packaging;
                        }
                        break;

                    case 'place_of_item_activity:placeOfItemActivityModule':
                        $countryCodes = json_decode(file_get_contents(__DIR__ . '/../gs1codes/countrycodes.json'), true);
                        $data['countryOfOrigin'] = $countryCodes[(string)$section->getElementsByTagName('countryCode')->item(0)->nodeValue];
                        break;

                    case 'sales_information:salesInformationModule':
                        $comparisonCodes = json_decode(file_get_contents(__DIR__ . '/../gs1codes/pricecomparisoncodes.json'), true);
                        /** @var \DOMElement $measurement */
                        $measurement = $section->getElementsByTagName('priceComparisonMeasurement')->item(0);
                        if ($measurement) {
                            $data['priceComparison'] = $measurement->nodeValue . ' ' . self::getMeasurement($measurement->getAttribute('measurementUnitCode'))
                                . ' ' .$comparisonCodes[strtolower($section->getElementsByTagName('priceComparisonContentTypeCode')->item(0)->nodeValue ?? self::VALUE_NOT_AVAILABLE)];
                        }
                        break;

                    case 'trade_item_lifespan:tradeItemLifespanModule':
                        $data['minimumLifespan'] = $section->getElementsByTagName('minimumTradeItemLifespanFromTimeOfProduction')->item(0)->nodeValue;
                        break;

                    case 'trade_item_measurements:tradeItemMeasurementsModule':
                        $data['measurements'] = ['grossWeight' => []];
                        foreach (['depth', 'height', 'netContent', 'width', 'tradeItemCompositionWidth'] as $dimension) {
                            $node = $section->getElementsByTagName($dimension);
                            if (count($node)) {
                                /** @var \DOMElement $nodeElement */
                                $nodeElement = $node->item(0);
                                $data['measurements'][$dimension] = $nodeElement->nodeValue . ' '
                                    . self::getMeasurement($nodeElement->getAttribute('measurementUnitCode'));
                            }
                        }

                        foreach ($section->getElementsByTagName('grossWeight') as $weight) {
                            /** @var \DOMElement $weight */
                            $data['measurements']['grossWeight'][] = $weight->nodeValue . ' ' . self::getMeasurement($weight->getAttribute('measurementUnitCode'));
                        }
                        break;

                    case 'trade_item_temperature_information:tradeItemTemperatureInformationModule':
                        $data['maximumTemperature'] = [];
                        $data['minimumTemperature'] = [];
                        foreach ($section->getElementsByTagName('maximumTemperature') as $maxTemp) {
                            /** @var \DOMElement $maxTemp */
                            $data['maximumTemperature'][] = $maxTemp->nodeValue . ' ' . self::getMeasurement($maxTemp->getAttribute('temperatureMeasurementUnitCode'));
                        }
                        foreach ($section->getElementsByTagName('minimumTemperature') as $minTemp) {
                            /** @var \DOMElement $minTemp */
                            $data['minimumTemperature'][] =  $minTemp->nodeValue . ' ' . self::getMeasurement($minTemp->getAttribute('temperatureMeasurementUnitCode'));
                        }
                        break;

                    case 'trade_item_handling:tradeItemHandlingModule':
                        $data['stackingFactor'] = $section->getElementsByTagName('stackingFactor')->item(0)->nodeValue;
                        break;

                    case 'trade_item_hierarchy:tradeItemHierarchyModule':
                        $data['packedIrregularly'] = strtolower($section->getElementsByTagName('isTradeItemPackedIrregularly')->item(0)->nodeValue ?? self::VALUE_NOT_AVAILABLE) === 'true';
                        $data['completeLayers'] = $section->getElementsByTagName('quantityOfCompleteLayersContainedInATradeItem')->item(0)->nodeValue ?? self::VALUE_NOT_AVAILABLE;
                        $data['itemsInCompleteLayer'] = $section->getElementsByTagName('quantityOfTradeItemsContainedInACompleteLayer')->item(0)->nodeValue ?? self::VALUE_NOT_AVAILABLE;
                        break;

                    case 'packaging_marking:packagingMarkingModule':
                        $data['packagingMarking'] = ['accreditationCodes' => []];
                        foreach ($section->childNodes as $childNode) {
                            /** @var \DOMElement $childNode */
                            if ($childNode->nodeName === 'isPackagingMarkedReturnable') {
                                $data['packagingMarking']['returnable'] = strtolower($childNode->nodeValue) === 'true';
                            } elseif ($childNode->nodeName === 'packagingMarkedLabelAccreditationCode') {
                                $data['packagingMarking']['accreditationCodes'][] = $childNode->nodeValue;
                            }
                        }
                        break;
                }
            } catch (\Throwable $e) {
                throw new ValidooApiException('Could not parse XML node "' . $nodeName . '".');
            }
        }
    }

    public static function fromArray(array $parameters): CatalogueItemNotification
    {
        return new CatalogueItemNotification($parameters);
    }

    private static function getMeasurement(string $code): string
    {
        $code = strtolower($code);

        $standardMeasurements = [
            'grm' => 'g',
            'kgm' => 'kg',
            'dlt' => 'dl',
            'ltr' => 'l',
            'gl' => 'g/l',
            'kjo' => 'kJ',
            'e14' => 'kcal',
            'mmt' => 'mm',
            'cmt' => 'cm',
            'mtr' => 'm',
            'cel' => 'Celsius',
            'fah' => 'Fahrenheit',
            'kel' => 'Kelvin'
        ];

        if (isset($standardMeasurements[$code])) {
            return $standardMeasurements[$code];
        }

        if (!isset(self::$measurementCodes)) {
            self::$measurementCodes = json_decode(file_get_contents(__DIR__ . '/../gs1codes/measurementcodes.json'), true);
        }

        return self::$measurementCodes[$code];
    }

    /**
     * @return array
     */
    public static function getSupportedData(): array
    {
        return [
            'allergens' => self::VALUE_STRING,
            'consumerUsageInstructions_sv' => self::VALUE_STRING,
            'descriptionShort_sv' => self::VALUE_STRING,
            'ingredientStatement_sv' => self::VALUE_STRING,
            'tradeItemMarketingMessage_sv' => self::VALUE_STRING,
            'nutritional.energy' => self::VALUE_STRING,
            'nutritional.fat' => self::VALUE_STRING,
            'nutritional.ofWhichSaturatedFat' => self::VALUE_STRING,
            'nutritional.carbohydrate' => self::VALUE_STRING,
            'nutritional.ofWhichSugars' => self::VALUE_STRING,
            'nutritional.protein' => self::VALUE_STRING,
            'nutritional.saltEquivalent' => self::VALUE_STRING,
            'packaging.0.packagingType' => self::VALUE_STRING,
            'packagingMarking.accreditationCodes' => self::VALUE_STRING,
            'countryOfOrigin' => self::VALUE_STRING,
            'priceComparison' => self::VALUE_STRING,
            'minimumLifespan' => self::VALUE_INTEGER,
            'measurements.grossWeight' => self::VALUE_STRING,
            'measurements.depth' => self::VALUE_STRING,
            'measurements.height' => self::VALUE_STRING,
            'measurements.netContent' => self::VALUE_STRING,
            'measurements.width' => self::VALUE_STRING,
            'maximumTemperature' => self::VALUE_STRING,
            'minimumTemperature' => self::VALUE_STRING,

            'case.0.gtin' => self::VALUE_STRING,
            'case.0.containerInfo.quantityOfChildren' => self::VALUE_INTEGER,
            'case.0.containerInfo.totalQuantityOfNextLowerLevelTradeItem' => self::VALUE_INTEGER,
            'case.0.packaging.0.packagingType' => self::VALUE_STRING,
            'case.0.packaging.0.packagingWeight' => self::VALUE_STRING,
            'case.0.packagingMarking.accreditationCodes' => self::VALUE_STRING,
            'case.0.stackingFactor' => self::VALUE_INTEGER,
            'case.0.packedIrregularly' => self::VALUE_BOOLEAN,
            'case.0.completeLayers' => self::VALUE_INTEGER,
            'case.0.itemsInCompleteLayer' => self::VALUE_INTEGER,
            'case.0.measurements.grossWeight' => self::VALUE_STRING,
            'case.0.measurements.depth' => self::VALUE_STRING,
            'case.0.measurements.height' => self::VALUE_STRING,
            'case.0.measurements.width' => self::VALUE_STRING,
            'case.0.measurements.tradeItemCompositionWidth' => self::VALUE_STRING,
            'case.0.maximumTemperature' => self::VALUE_STRING,
            'case.0.minimumTemperature' => self::VALUE_STRING,

            'case.1.gtin' => self::VALUE_STRING,
            'case.1.containerInfo.quantityOfChildren' => self::VALUE_INTEGER,
            'case.1.containerInfo.totalQuantityOfNextLowerLevelTradeItem' => self::VALUE_INTEGER,
            'case.1.packaging.0.packagingType' => self::VALUE_STRING,
            'case.1.packaging.0.packagingWeight' => self::VALUE_STRING,
            'case.1.packagingMarking.accreditationCodes' => self::VALUE_STRING,
            'case.1.stackingFactor' => self::VALUE_INTEGER,
            'case.1.packedIrregularly' => self::VALUE_BOOLEAN,
            'case.1.completeLayers' => self::VALUE_INTEGER,
            'case.1.itemsInCompleteLayer' => self::VALUE_INTEGER,
            'case.1.measurements.grossWeight' => self::VALUE_STRING,
            'case.1.measurements.depth' => self::VALUE_STRING,
            'case.1.measurements.height' => self::VALUE_STRING,
            'case.1.measurements.width' => self::VALUE_STRING,
            'case.1.measurements.tradeItemCompositionWidth' => self::VALUE_STRING,
            'case.1.maximumTemperature' => self::VALUE_STRING,
            'case.1.minimumTemperature' => self::VALUE_STRING,

            'pallet.gtin' => self::VALUE_STRING,
            'pallet.containerInfo.quantityOfChildren' => self::VALUE_INTEGER,
            'pallet.containerInfo.totalQuantityOfNextLowerLevelTradeItem' => self::VALUE_INTEGER,
            'pallet.packaging.0.packagingType' => self::VALUE_STRING,
            'pallet.packagingMarking.accreditationCodes' => self::VALUE_STRING,
            'pallet.stackingFactor' => self::VALUE_INTEGER,
            'pallet.packedIrregularly' => self::VALUE_BOOLEAN,
            'pallet.completeLayers' => self::VALUE_INTEGER,
            'pallet.itemsInCompleteLayer' => self::VALUE_INTEGER,
            'pallet.measurements.grossWeight' => self::VALUE_STRING,
            'pallet.measurements.depth' => self::VALUE_STRING,
            'pallet.measurements.height' => self::VALUE_STRING,
            'pallet.measurements.width' => self::VALUE_STRING,
        ];
    }

    private static function extractLanguageVariants(\DOMElement $node, string $tagName, array &$data)
    {
        foreach ($node->getElementsByTagName($tagName) as $languageVariant) {
            /** @var \DOMElement $languageVariant */
            $data[$tagName . '_' . $languageVariant->getAttribute('languageCode')] = $languageVariant->nodeValue;
        }
    }

    /**
     * @return string
     */
    public function getUnitDescriptorCode(): string
    {
        return $this->unitDescriptorCode;
    }
}
