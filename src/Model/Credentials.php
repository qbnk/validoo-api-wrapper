<?php

declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model;

class Credentials implements \JsonSerializable
{

    protected string $clientId;
    protected string $clientSecret;
    protected string $username;
    protected string $password;
    protected array $scope;

    /**
     * @return string
     */
    public function getClientId(): string
    {
        return $this->clientId;
    }

    /**
     * @param string $clientId
     * @return Credentials
     */
    public function setClientId(string $clientId): Credentials
    {
        $this->clientId = $clientId;
        return $this;
    }

    /**
     * @return string
     */
    public function getClientSecret(): string
    {
        return $this->clientSecret;
    }

    /**
     * @param string $clientSecret
     * @return Credentials
     */
    public function setClientSecret(string $clientSecret): Credentials
    {
        $this->clientSecret = $clientSecret;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return Credentials
     */
    public function setUsername(string $username): Credentials
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword(): string
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return Credentials
     */
    public function setPassword(string $password): Credentials
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getScope(): array
    {
        return $this->scope ?? [];
    }

    /**
     * Available scopes: digitalassets.api tradeitem.api subscriptions.api offline_access
     * “offline_access” must have been included in the scope of the request to be able to refresh the token.
     * @param string[] $scope
     * @return Credentials
     */
    public function setScope(array $scope): Credentials
    {
        $this->scope = $scope;
        return $this;
    }

    public static function fromArray(array $parameters): Credentials
    {
        $token = new self();

        $token->setClientId($parameters['clientId']);
        $token->setClientSecret($parameters['clientSecret']);
        $token->setUsername($parameters['username']);
        $token->setPassword($parameters['password']);
        $token->setScope(is_array($parameters['scope']) ? $parameters['scope'] : explode(',', $parameters['scope']));

        return $token;
    }

    public function jsonSerialize(): array
    {
        return [
            'clientId' => $this->getClientId(),
            'clientSecret' => $this->getClientSecret(),
            'username' => $this->getUsername(),
            'password' => $this->getPassword(),
            'scope' => implode(',', $this->getScope())
        ];
    }
}