<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model\Enum;

use ReflectionClass;

abstract class BasicEnum
{
    /** @var string[] */
    private static array $valueCacheArray;

    private static function getValues(): array
    {
        if (self::$valueCacheArray === null) {
            $reflect = new ReflectionClass(self::class);
            self::$valueCacheArray = $reflect->getConstants();
        }

        return self::$valueCacheArray;
    }

    public static function isValidValue($value, $strict = true): bool
    {
        return in_array($value, self::getValues(), $strict);
    }
}