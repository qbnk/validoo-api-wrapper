<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model\Enum;

abstract class CinFormat extends BasicEnum
{
    /**
     * Never booked any QA
     * @var string
     */
    public const XML = 'Xml';

    /**
     * Booked, but no results yet
     * @var string
     */
    public const TEXT = 'Text';
}