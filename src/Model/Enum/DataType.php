<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model\Enum;

abstract class DataType extends BasicEnum
{
    public const PRODUCT = 'Product';

    public const GDSN = 'Gdsn';
}