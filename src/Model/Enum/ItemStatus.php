<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model\Enum;

abstract class ItemStatus extends BasicEnum
{
    /**
     * @var string
     */
    public const PUBLISHED = 'Published';

    /**
     * @var string
     */
    public const DISCONTINUED = 'Discontinued';
}