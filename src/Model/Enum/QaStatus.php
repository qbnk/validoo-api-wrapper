<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model\Enum;

abstract class QaStatus extends BasicEnum
{
    /**
     * If this parameter is not combined with either ItemQAResultStatus or AssetCategory,
     * then all itemQAResultStatus (Digital, Measurement, Barcode) must be OK for item to be returned.
     * If the parameter is combined with itemQAResultStatus only that must be OK.
     * If the parameter is combined with assetCategory all itemQAResultStatus in that category must be OK.
     * @var string
     */
    public const OK = 'OK';

    /**
     * At least one itemQAResultStatus must be ActionRequired for item to be returned
     * If the parameter is combined with itemQAResultStatus that must be ActionRequired.
     * @var string
     */
    public const ACTION_REQUIRED = 'ActionRequired';

    /**
     * Never booked any QA
     * @var string
     */
    public const NA = 'N/A';

    /**
     * Booked, but no results yet
     * @var string
     */
    public const BOOKED = 'Booked';
}