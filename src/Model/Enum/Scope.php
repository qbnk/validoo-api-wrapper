<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model\Enum;

abstract class Scope extends BasicEnum
{
    public const DIGITAL_ASSETS = 'digitalassets.api';

    public const TRADE_ITEM = 'tradeitem.api';

    public const SUBSCRIPTIONS = 'subscriptions.api';

    public const OFFLINE_ACCESS = 'offline_access';
}