<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model\Enum;

abstract class SearchSort extends BasicEnum
{
    /**
     * Default
     */
    public const CHANGE_DATE = 'lastChangeDateTime';

    public const GTIN = 'gtin';

    public const BRAND_NAME = 'brandName';

    public const PROVIDER_NAME = 'informationProviderName';
}