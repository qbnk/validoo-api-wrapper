<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model\Enum;

abstract class UnitDescriptorCode extends BasicEnum
{
    public const BASE_UNIT_OR_EACH = 'BASE_UNIT_OR_EACH';

    public const CASE = 'CASE';

    public const MIXED_MODULE = 'MIXED_MODULE';

    public const PACK_OR_INNER_PACK = 'PACK_OR_INNER_PACK';

    public const PALLET = 'PALLET';

    public const TRANSPORT_LOAD = 'TRANSPORT_LOAD';
}