<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model;

class ItemQaStatus implements \JsonSerializable
{

    /**
     * Result status for Digital QA – If this parameter is used
     * without the combination of itemQAStatus, all items with
     * DigitalQAStatus OK or ActionRequired are returned.
     * @var string One of QaStatus::constant
     */
    protected string $digitalQAStatus;

    protected ?\DateTime $digitalQAStatusChangeDateTime;

    /**
     * Result status for Measurement QA - If this parameter is
     * used without the combination of itemQAStatus, all items
     * with MeasurementQAStatus OK or ActionRequired are
     * returned.
     * @var string One of QaStatus::constant
     */
    protected string $measurementQAStatus;

    protected ?\DateTime $measurementQAStatusChangeDateTime;

    /**
     * Result status for Barcode QA - If this parameter is used
     * without the combination of itemQAStatus, all items with
     * BarcodeQAStatus OK or ActionRequired are returned.
     * @var string One of QaStatus::constant
     */
    protected string $barcodeQAStatus;

    protected ?\DateTime $barcodeQAStatusChangeDateTime;

    protected ?\DateTime $itemQAResultStatusChangeDateTime;

    public static function fromArray(array $parameters): ItemQaStatus
    {
        $status = new self();

        foreach ($parameters as $property => $value) {
            $property = ucfirst($property);
            if ($value !== null && method_exists($status, 'set' . $property)) {
                $status->{"set{$property}"}(strpos($property, 'Date') ? new \DateTime($value) : $value);
            }
        }

        return $status;
    }

    /**
     * @return string
     */
    public function getDigitalQAStatus(): string
    {
        return $this->digitalQAStatus;
    }

    /**
     * @param string $digitalQAStatus
     * @return ItemQaStatus
     */
    public function setDigitalQAStatus(string $digitalQAStatus): ItemQaStatus
    {
        $this->digitalQAStatus = $digitalQAStatus;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getDigitalQAStatusChangeDateTime():? \DateTime
    {
        return $this->digitalQAStatusChangeDateTime ?? null;
    }

    /**
     * @param \DateTime $digitalQAStatusChangeDateTime
     * @return ItemQaStatus
     */
    public function setDigitalQAStatusChangeDateTime(\DateTime $digitalQAStatusChangeDateTime): ItemQaStatus
    {
        $this->digitalQAStatusChangeDateTime = $digitalQAStatusChangeDateTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getMeasurementQAStatus(): string
    {
        return $this->measurementQAStatus;
    }

    /**
     * @param string $measurementQAStatus
     * @return ItemQaStatus
     */
    public function setMeasurementQAStatus(string $measurementQAStatus): ItemQaStatus
    {
        $this->measurementQAStatus = $measurementQAStatus;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getMeasurementQAStatusChangeDateTime():? \DateTime
    {
        return $this->measurementQAStatusChangeDateTime ?? null;
    }

    /**
     * @param \DateTime $measurementQAStatusChangeDateTime
     * @return ItemQaStatus
     */
    public function setMeasurementQAStatusChangeDateTime(\DateTime $measurementQAStatusChangeDateTime): ItemQaStatus
    {
        $this->measurementQAStatusChangeDateTime = $measurementQAStatusChangeDateTime;
        return $this;
    }

    /**
     * @return string
     */
    public function getBarcodeQAStatus(): string
    {
        return $this->barcodeQAStatus;
    }

    /**
     * @param string $barcodeQAStatus
     * @return ItemQaStatus
     */
    public function setBarcodeQAStatus(string $barcodeQAStatus): ItemQaStatus
    {
        $this->barcodeQAStatus = $barcodeQAStatus;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getBarcodeQAStatusChangeDateTime():? \DateTime
    {
        return $this->barcodeQAStatusChangeDateTime ?? null;
    }

    /**
     * @param \DateTime $barcodeQAStatusChangeDateTime
     * @return ItemQaStatus
     */
    public function setBarcodeQAStatusChangeDateTime(\DateTime $barcodeQAStatusChangeDateTime): ItemQaStatus
    {
        $this->barcodeQAStatusChangeDateTime = $barcodeQAStatusChangeDateTime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getItemQAResultStatusChangeDateTime():? \DateTime
    {
        return $this->itemQAResultStatusChangeDateTime ?? null;
    }

    /**
     * @param \DateTime $itemQAResultStatusChangeDateTime
     * @return ItemQaStatus
     */
    public function setItemQAResultStatusChangeDateTime(\DateTime $itemQAResultStatusChangeDateTime): ItemQaStatus
    {
        $this->itemQAResultStatusChangeDateTime = $itemQAResultStatusChangeDateTime;
        return $this;
    }

    public function jsonSerialize(): array
    {
        $json = [
            'digitalQAStatus' => $this->getDigitalQAStatus(),
            'measurementQAStatus' => $this->getMeasurementQAStatus(),
            'barcodeQAStatus' => $this->getBarcodeQAStatus()
        ];

        foreach([
            'digitalQAStatusChangeDateTime',
            'measurementQAStatusChangeDateTime',
            'barcodeQAStatusChangeDateTime',
            'itemQAResultStatusChangeDateTime'
        ] as $dateProperty) {
            $value = $this->{"get{$dateProperty}"}();
            if ($value instanceof \DateTime) {
                $json[$dateProperty] = $value->format(Product::DATE_FORMAT);
            }
        }

        return $json;
    }
}