<?php

declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model;

class Product implements \JsonSerializable
{
    public const DATE_FORMAT = 'Y-m-d\TH:i:s.v\Z';

  /**
   * Product GTIN (Global Trade Item Number)
   * @var string
   */
    protected string $gtin = '';

  /**
   * Identity, according to GS1's identification system, of the party who has created the business document.
   * @var string
   */
    protected string $informationProviderGln = '';

  /**
   * Name of the party who has created the business document
   * @var string
   */
    protected string $informationProviderName = '';

    protected string $targetMarketCountryCode = '';

    protected string $targetMarketName = '';

  /**
   * Identity of the brand owner according to GS1's identification system.
   * @var string
   */
    protected string $brandOwnerGln = '';

  /**
   * Name of the party who owns the brand of the trade item.
   * @var string
   */
    protected string $brandOwnerName = '';

    protected string $descriptiveSize = '';

    protected string $brandName = '';

  /**
   * Code indicating the classification category (brick code) for the trade item according to GPC.
   * Example 10000051=Vinegars, 10000159=Beer
   * @var string
   */
    protected string $gpcCode = '';

  /**
   * A trade item at base unit level contains no other items identified with a GTIN
   * @var bool
   */
    protected bool $isTradeItemABaseUnit = false;

  /**
   * The trade item is intended to be sold to a consumer or end user at point of sale.
   * @var bool
   */
    protected bool $isTradeItemAConsumerUnit = false;

  /**
   * The trade item is ordered from the supplier by a wholesaler, store, or similar.
   * @var bool
   */
    protected bool $isTradeItemAnOrderableUnit = false;

    protected bool $isTradeItemADisplayUnit = false;

  /**
   * The trade item, usually a pallet, that is physically delivered from the supplier to the customer.
   * @var bool
   */
    protected bool $isTradeItemADespatchUnit = false;

  /**
   * Date and point of time when the business document was created.
   * @var \DateTime|null
   */
    protected ?\DateTime $creationDateTime;

  /**
   * Date and point of time from which the information in this business document is effective.
   * @var \DateTime|null
   */
    protected ?\DateTime $effectiveDateTime;

  /**
   * Date and point of time from when the item is available.
   * @var \DateTime|null
   */
    protected ?\DateTime $startAvailabilityDate;

  /**
   * The first date and point of time at which a trade item can be sold to consumers.
   * @var \DateTime|null
   */
    protected ?\DateTime $consumerFirstAvailabilityDateTime;

  /**
   * Date and point of time on which the Trade Item Information last was changed in the information provider's system.
   * @var \DateTime|null
   */
    protected ?\DateTime $lastChangeDateTime;

  /**
   * The earliest date and point of time that the trade item can
   * be shipped according to the delivery terms, independent of
   * any specific ship-from location.
   * @var \DateTime|null
   */
    protected ?\DateTime $firstShipDateTime;

  /**
   * The name of this trade item.
   * @var string
   */
    protected string $functionalName = '';

    protected ?ItemQaStatus $qaStatus = null;

    protected ?int $itemId;

  /**
   * One of ItemStatus::constant
   * @var string|null
   */
    protected ?string $status;

    protected bool $topItem = false;

    protected string $supplierArticleNumber = '';

    protected ?CatalogueItemNotification $cin;

    public static function fromArray(array $parameters): Product
    {
        $product = new self();

        foreach ($parameters as $property => $value) {
            $property = ucfirst($property);
            if ($property === 'QaStatus') {
                $product->setQaStatus(ItemQaStatus::fromArray($value));
            } elseif ($property === 'Cin') {
                $product->setCin(is_array($value)
                ? CatalogueItemNotification::fromArray($value)
                : CatalogueItemNotification::fromPayload($value));
            } elseif ($value !== null && method_exists($product, 'set' . $property)) {
                $product->{"set{$property}"}(strpos($property, 'Date') ? new \DateTime($value) : $value);
            }
        }

        return $product;
    }

  /**
   * @return string
   */
    public function getGtin(): string
    {
        return $this->gtin;
    }

  /**
   * @param string $gtin
   * @return Product
   */
    public function setGtin(string $gtin): Product
    {
        $this->gtin = $gtin;
        return $this;
    }

  /**
   * @return string
   */
    public function getInformationProviderGln(): string
    {
        return $this->informationProviderGln;
    }

  /**
   * @param string $informationProviderGln
   * @return Product
   */
    public function setInformationProviderGln(string $informationProviderGln): Product
    {
        $this->informationProviderGln = $informationProviderGln;
        return $this;
    }

  /**
   * @return string
   */
    public function getInformationProviderName(): string
    {
        return $this->informationProviderName;
    }

  /**
   * @param string $informationProviderName
   * @return Product
   */
    public function setInformationProviderName(string $informationProviderName): Product
    {
        $this->informationProviderName = $informationProviderName;
        return $this;
    }

  /**
   * @return string
   */
    public function getTargetMarketCountryCode(): string
    {
        return $this->targetMarketCountryCode;
    }

  /**
   * @param string $targetMarketCountryCode
   * @return Product
   */
    public function setTargetMarketCountryCode(string $targetMarketCountryCode): Product
    {
        $this->targetMarketCountryCode = $targetMarketCountryCode;
        return $this;
    }

  /**
   * @return string
   */
    public function getTargetMarketName(): string
    {
        return $this->targetMarketName;
    }

  /**
   * @param string $targetMarketName
   * @return Product
   */
    public function setTargetMarketName(string $targetMarketName): Product
    {
        $this->targetMarketName = $targetMarketName;
        return $this;
    }

  /**
   * @return string
   */
    public function getBrandOwnerGln(): string
    {
        return $this->brandOwnerGln;
    }

  /**
   * @param string $brandOwnerGln
   * @return Product
   */
    public function setBrandOwnerGln(string $brandOwnerGln): Product
    {
        $this->brandOwnerGln = $brandOwnerGln;
        return $this;
    }

  /**
   * @return string
   */
    public function getBrandOwnerName(): string
    {
        return $this->brandOwnerName;
    }

  /**
   * @param string $brandOwnerName
   * @return Product
   */
    public function setBrandOwnerName(string $brandOwnerName): Product
    {
        $this->brandOwnerName = $brandOwnerName;
        return $this;
    }

  /**
   * @return string
   */
    public function getDescriptiveSize(): string
    {
        return $this->descriptiveSize;
    }

  /**
   * @param string $descriptiveSize
   * @return Product
   */
    public function setDescriptiveSize(string $descriptiveSize): Product
    {
        $this->descriptiveSize = $descriptiveSize;
        return $this;
    }

  /**
   * @return string
   */
    public function getBrandName(): string
    {
        return $this->brandName;
    }

  /**
   * @param string $brandName
   * @return Product
   */
    public function setBrandName(string $brandName): Product
    {
        $this->brandName = $brandName;
        return $this;
    }

  /**
   * @return string
   */
    public function getGpcCode(): string
    {
        return $this->gpcCode;
    }

  /**
   * @param string $gpcCode
   * @return Product
   */
    public function setGpcCode(string $gpcCode): Product
    {
        $this->gpcCode = $gpcCode;
        return $this;
    }

  /**
   * @return bool
   */
    public function getIsTradeItemABaseUnit(): bool
    {
        return $this->isTradeItemABaseUnit;
    }

  /**
   * @param bool $isTradeItemABaseUnit
   * @return Product
   */
    public function setIsTradeItemABaseUnit(bool $isTradeItemABaseUnit): Product
    {
        $this->isTradeItemABaseUnit = $isTradeItemABaseUnit;
        return $this;
    }

  /**
   * @return bool
   */
    public function getIsTradeItemAConsumerUnit(): bool
    {
        return $this->isTradeItemAConsumerUnit;
    }

  /**
   * @param bool $isTradeItemAConsumerUnit
   * @return Product
   */
    public function setIsTradeItemAConsumerUnit(bool $isTradeItemAConsumerUnit): Product
    {
        $this->isTradeItemAConsumerUnit = $isTradeItemAConsumerUnit;
        return $this;
    }

  /**
   * @return bool
   */
    public function getIsTradeItemAnOrderableUnit(): bool
    {
        return $this->isTradeItemAnOrderableUnit;
    }

  /**
   * @param bool $isTradeItemAnOrderableUnit
   * @return Product
   */
    public function setIsTradeItemAnOrderableUnit(bool $isTradeItemAnOrderableUnit): Product
    {
        $this->isTradeItemAnOrderableUnit = $isTradeItemAnOrderableUnit;
        return $this;
    }

  /**
   * @return bool
   */
    public function getIsTradeItemADisplayUnit(): bool
    {
        return $this->isTradeItemADisplayUnit;
    }

  /**
   * @param bool $isTradeItemADisplayUnit
   * @return Product
   */
    public function setIsTradeItemADisplayUnit(bool $isTradeItemADisplayUnit): Product
    {
        $this->isTradeItemADisplayUnit = $isTradeItemADisplayUnit;
        return $this;
    }

  /**
   * The trade item, usually a pallet, that is physically delivered from the supplier to the customer.
   * @return bool
   */
    public function getIsTradeItemADespatchUnit(): bool
    {
        return $this->isTradeItemADespatchUnit;
    }

  /**
   * @param bool $isTradeItemADisplayUnit
   * @return Product
   */
    public function setIsTradeItemADespatchUnit(bool $isTradeItemADisplayUnit): Product
    {
        $this->isTradeItemADespatchUnit = $isTradeItemADisplayUnit;
        return $this;
    }

  /**
   * @return \DateTime|null
   */
    public function getCreationDateTime(): ?\DateTime
    {
        return $this->creationDateTime ?? null;
    }

  /**
   * @param \DateTime $creationDateTime
   * @return Product
   */
    public function setCreationDateTime(\DateTime $creationDateTime): Product
    {
        $this->creationDateTime = $creationDateTime;
        return $this;
    }

  /**
   * @return \DateTime|null
   */
    public function getEffectiveDateTime(): ?\DateTime
    {
        return $this->effectiveDateTime ?? null;
    }

  /**
   * @param \DateTime $effectiveDateTime
   * @return Product
   */
    public function setEffectiveDateTime(\DateTime $effectiveDateTime): Product
    {
        $this->effectiveDateTime = $effectiveDateTime;
        return $this;
    }

  /**
   * @return \DateTime|null
   */
    public function getStartAvailabilityDate(): ?\DateTime
    {
        return $this->startAvailabilityDate ?? null;
    }

  /**
   * @param \DateTime $startAvailabilityDate
   * @return Product
   */
    public function setStartAvailabilityDate(\DateTime $startAvailabilityDate): Product
    {
        $this->startAvailabilityDate = $startAvailabilityDate;
        return $this;
    }

  /**
   * @return \DateTime|null
   */
    public function getConsumerFirstAvailabilityDateTime(): ?\DateTime
    {
        return $this->consumerFirstAvailabilityDateTime ?? null;
    }

  /**
   * @param \DateTime $consumerFirstAvailabilityDateTime
   * @return Product
   */
    public function setConsumerFirstAvailabilityDateTime(\DateTime $consumerFirstAvailabilityDateTime): Product
    {
        $this->consumerFirstAvailabilityDateTime = $consumerFirstAvailabilityDateTime;
        return $this;
    }

  /**
   * @return \DateTime|null
   */
    public function getLastChangeDateTime(): ?\DateTime
    {
        return $this->lastChangeDateTime ?? null;
    }

  /**
   * @param \DateTime $lastChangeDateTime
   * @return Product
   */
    public function setLastChangeDateTime(\DateTime $lastChangeDateTime): Product
    {
        $this->lastChangeDateTime = $lastChangeDateTime;
        return $this;
    }

  /**
   * @return \DateTime|null
   */
    public function getFirstShipDateTime(): ?\DateTime
    {
        return $this->firstShipDateTime ?? null;
    }

  /**
   * @param \DateTime $firstShipDateTime
   * @return Product
   */
    public function setFirstShipDateTime(\DateTime $firstShipDateTime): Product
    {
        $this->firstShipDateTime = $firstShipDateTime;
        return $this;
    }

  /**
   * @return string
   */
    public function getFunctionalName(): string
    {
        return $this->functionalName;
    }

  /**
   * @param string $functionalName
   * @return Product
   */
    public function setFunctionalName(string $functionalName): Product
    {
        $this->functionalName = $functionalName;
        return $this;
    }

  /**
   * @return ItemQaStatus
   */
    public function getQaStatus(): ?ItemQaStatus
    {
        return $this->qaStatus;
    }

  /**
   * @param ItemQaStatus $qaStatus
   * @return Product
   */
    public function setQaStatus(ItemQaStatus $qaStatus): Product
    {
        $this->qaStatus = $qaStatus;
        return $this;
    }

  /**
   * @return int
   */
    public function getItemId(): int
    {
        return $this->itemId;
    }

  /**
   * @param int $itemId
   * @return Product
   */
    public function setItemId(int $itemId): Product
    {
        $this->itemId = $itemId;
        return $this;
    }

  /**
   * @return string
   */
    public function getStatus(): string
    {
        return $this->status;
    }

  /**
   * @param string $status
   * @return Product
   */
    public function setStatus(string $status): Product
    {
        $this->status = $status;
        return $this;
    }

  /**
   * @return bool
   */
    public function getTopItem(): bool
    {
        return $this->topItem;
    }

  /**
   * @param bool $topItem
   * @return Product
   */
    public function setTopItem(bool $topItem): Product
    {
        $this->topItem = $topItem;
        return $this;
    }

  /**
   * @return string
   */
    public function getSupplierArticleNumber(): string
    {
        return $this->supplierArticleNumber;
    }

  /**
   * @param string $supplierArticleNumber
   * @return Product
   */
    public function setSupplierArticleNumber(string $supplierArticleNumber): Product
    {
        $this->supplierArticleNumber = $supplierArticleNumber;
        return $this;
    }

  /**
   * @return CatalogueItemNotification|null
   */
    public function getCin(): ?CatalogueItemNotification
    {
        return $this->cin;
    }

  /**
   * @param CatalogueItemNotification $cin
   * @return Product
   */
    public function setCin(CatalogueItemNotification $cin): Product
    {
        $this->cin = $cin;
        return $this;
    }

    public function jsonSerialize(): array
    {
        $json = [
        'gtin' => $this->getGtin(),
        'informationProviderGln' => $this->getInformationProviderGln(),
        'informationProviderName' => $this->getInformationProviderName(),
        'targetMarketCountryCode' => $this->getTargetMarketCountryCode(),
        'targetMarketName' => $this->getTargetMarketName(),
        'brandOwnerGln' => $this->getBrandOwnerGln(),
        'brandOwnerName' => $this->getBrandOwnerName(),
        'descriptiveSize' => $this->getDescriptiveSize(),
        'brandName' => $this->getBrandName(),
        'gpcCode' => $this->getGpcCode(),
        'isTradeItemABaseUnit' => $this->getIsTradeItemABaseUnit(),
        'isTradeItemAConsumerUnit' => $this->getIsTradeItemAConsumerUnit(),
        'isTradeItemAnOrderableUnit' => $this->getIsTradeItemAnOrderableUnit(),
        'isTradeItemADisplayUnit' => $this->getIsTradeItemADisplayUnit(),
        'itemId' => $this->getItemId(),
        'status' => $this->getStatus(),
        'topItem' => $this->getTopItem(),
        'supplierArticleNumber' => $this->getSupplierArticleNumber(),
        'functionalName' => $this->getFunctionalName(),
        'qaStatus' => $this->qaStatus ?? null,
        'cin' => $this->cin ?? null,
        ];

        foreach (
            [
            'creationDateTime',
            'effectiveDateTime',
            'startAvailabilityDate',
            'consumerFirstAvailabilityDateTime',
            'lastChangeDateTime',
            'firstShipDateTime'
            ] as $dateProperty
        ) {
            if (!isset($this->{$dateProperty})) {
                continue;
            }
            $value = $this->{"get{$dateProperty}"}();
            if ($value instanceof \DateTime) {
                $json[$dateProperty] = $value->format(self::DATE_FORMAT);
            }
        }

        return $json;
    }
}
