<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model;

use QBNK\Integration\Validoo\Model\Enum\DataType;
use QBNK\Integration\Validoo\Model\Enum\SearchSort;

class Search implements \JsonSerializable
{
    protected ?\DateTime $lastChangeFrom = null;

    protected ?\DateTime $lastChangeTo = null;

    protected string $dataType = DataType::PRODUCT;

    /** @var string[] */
    protected array $gtins = [];

    protected string $sortBy = SearchSort::CHANGE_DATE;

    /**
     * @return \DateTime|null
     */
    public function getLastChangeFrom(): ?\DateTime
    {
        return $this->lastChangeFrom;
    }

    /**
     * @param \DateTime|null $lastChangeFrom
     * @return Search
     */
    public function setLastChangeFrom(?\DateTime $lastChangeFrom): Search
    {
        $this->lastChangeFrom = $lastChangeFrom;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getLastChangeTo(): ?\DateTime
    {
        return $this->lastChangeTo;
    }

    /**
     * @param \DateTime|null $lastChangeTo
     * @return Search
     */
    public function setLastChangeTo(?\DateTime $lastChangeTo): Search
    {
        $this->lastChangeTo = $lastChangeTo;
        return $this;
    }

    /**
     * Product // Gdsn
     * @return string
     */
    public function getDataType(): string
    {
        return $this->dataType;
    }

    /**
     * @param string $dataType
     * @return Search
     */
    public function setDataType(string $dataType): Search
    {
        $this->dataType = $dataType;
        return $this;
    }

    /**
     * @return string
     */
    public function getSortBy(): string
    {
        return $this->sortBy;
    }

    /**
     * @param string $sortBy
     * @return Search
     */
    public function setSortBy(string $sortBy): Search
    {
        $this->sortBy = $sortBy;
        return $this;
    }

    /**
     * @return string[]
     */
    public function getGtins(): array
    {
        return $this->gtins;
    }

    /**
     * @param string[] $gtins
     * @return Search
     */
    public function setGtins(array $gtins): Search
    {
        $this->gtins = $gtins;
        return $this;
    }

    public function jsonSerialize(): array
    {
        $json = [
            'sortBy' => $this->getSortBy(),
            'sortAscending' => true,
            'dataType' => $this->getDataType()
        ];

        $lastChangeFrom = $this->getLastChangeFrom();
        if ($lastChangeFrom) {
            $json['lastChangeDateTimeFrom'] = $lastChangeFrom->format(DATE_ATOM);
        }

        $lastChangeTo = $this->getLastChangeTo();
        if ($lastChangeTo) {
            $json['lastChangeDateTimeTo'] = $lastChangeTo->format(DATE_ATOM);
        }

        if (!empty($this->getGtins())) {
            $json['gtins'] = $this->getGtins();
        }

        return $json;
    }
}