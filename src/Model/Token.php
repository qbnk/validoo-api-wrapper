<?php

declare(strict_types=1);

namespace QBNK\Integration\Validoo\Model;

class Token implements \JsonSerializable
{

    protected string $accessToken;
    protected string $refreshToken;
    protected \DateTime $expires;
    protected string $tokenType;
    protected array $scope;

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return $this->accessToken;
    }

    /**
     * @return string
     */
    public function getRefreshToken(): string
    {
        return $this->refreshToken;
    }

    /**
     * @return \DateTime
     */
    public function getExpires(): \DateTime
    {
        return $this->expires;
    }

    /**
     * @param \DateTime $expires
     * @return Token
     */
    public function setExpires(\DateTime $expires): self
    {
        $this->expires = $expires;
        return $this;
    }

    /**
     * "Bearer"
     * @return string
     */
    public function getTokenType(): string
    {
        return $this->tokenType;
    }

    /**
     * @return string[]
     */
    public function getScope(): array
    {
        return $this->scope ?? [];
    }

    /**
     * @param string $accessToken
     * @return Token
     */
    public function setAccessToken(string $accessToken): self
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @param string $refreshToken
     * @return Token
     */
    public function setRefreshToken(string $refreshToken): self
    {
        $this->refreshToken = $refreshToken;
        return $this;
    }

    /**
     * @param string $tokenType
     * @return Token
     */
    public function setTokenType(string $tokenType): self
    {
        $this->tokenType = $tokenType;
        return $this;
    }

    /**
     * Available scopes: digitalassets.api tradeitem.api subscriptions.api offline_access
     * “offline_access” must have been included in the scope of the request to be able to refresh the token.
     * @param string[] $scope
     * @return Token
     */
    public function setScope(array $scope): self
    {
        $this->scope = $scope;
        return $this;
    }

    public static function fromArray(array $parameters): Token
    {
        $token = new self();

        $token->setAccessToken($parameters['accessToken']);
        $token->setRefreshToken($parameters['refreshToken']);
        $token->setExpires($parameters['expires'] instanceof \DateTime ? $parameters['expires'] : new \DateTime($parameters['expires']));
        $token->setScope(is_array($parameters['scope']) ? $parameters['scope'] : explode(',', $parameters['scope']));
        $token->setTokenType($parameters['tokenType']);

        return $token;
    }

    public function jsonSerialize(): array
    {
        return [
            'accessToken' => $this->getAccessToken(),
            'refreshToken' => $this->getRefreshToken(),
            'expires' => $this->getExpires()->format(DATE_ATOM),
            'tokenType' => $this->getTokenType(),
            'scope' => implode(',', $this->getScope())
        ];
    }
}