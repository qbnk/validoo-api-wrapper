<?php
declare(strict_types=1);

namespace QBNK\Integration\Validoo;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7\Request;
use QBNK\Integration\Validoo\Model\Credentials;
use QBNK\Integration\Validoo\Model\Enum\CinFormat;
use QBNK\Integration\Validoo\Model\Enum\DataType;
use QBNK\Integration\Validoo\Model\Enum\UnitDescriptorCode;
use QBNK\Integration\Validoo\Model\Product;
use QBNK\Integration\Validoo\Model\Search;
use QBNK\Integration\Validoo\Model\Token;
use QBNK\Integration\Validoo\Model\ValidooApiException;

class ValidooApiWrapper
{
    protected Client $client;

    protected bool $preProd;

    protected Credentials $credentials;

    protected ?Token $accessToken;

    public function __construct(Credentials $credentials, array $options = [], bool $preProd = false)
    {
        $this->credentials = $credentials;
        $this->preProd = $preProd;
        $this->client = new Client(array_merge([
            'base_uri' => $this->preProd
                ? 'https://services-preprod.validoo.se/tradeitem.api/'
                : 'https://services.validoo.se/tradeitem.api/'
        ], $options));
    }

    /**
     * @return Token
     * @throws ValidooApiException
     */
    public function acquireToken(): Token
    {
        $response = $this->client->request(
            'POST',
            'https://identity' . ($this->preProd ? '-preprod' : '') . '.validoo.se/connect/token',
            [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => $this->credentials->getClientId(),
                    'client_secret' => $this->credentials->getClientSecret(),
                    'scope' => implode(' ', $this->credentials->getScope()),
                    'username' => $this->credentials->getUsername(),
                    'password' => $this->credentials->getPassword()
                ]
            ]
        );

        if ($response->getStatusCode() !== 200) {
            throw new ValidooApiException('Bad token response; ' . $response->getReasonPhrase(), $response->getStatusCode());
        }

        $responseToken = json_decode((string)$response->getBody(), true);

        $this->setAccessToken(
            (new Token())
                ->setAccessToken($responseToken['access_token'])
                ->setExpires((new \DateTime('+' . $responseToken['expires_in'] . ' seconds')))
                ->setTokenType($responseToken['token_type'])
                ->setRefreshToken($responseToken['refresh_token'])
                ->setScope(explode(' ', $responseToken['scope']))
        );

        return $this->getAccessToken();
    }

    /**
     * Search for validoo items. Responses do not have product CIN data!
     * @param Search $searchModel
     * @param int $offset
     * @param int $limit
     * @return Product[]
     * @throws ValidooApiException
     */
    public function search(Search $searchModel, int $offset = 0, int $limit = 50): array
    {
        $searchBody = $searchModel->jsonSerialize();
        $searchBody['limit'] = $limit;
        $searchBody['skip'] = $offset;

        $response = $this->client->send(new Request(
            'POST',
            'TradeItemInformation/search',
            [
                'Authorization' => $this->getAccessToken()->getTokenType() . ' ' . $this->getAccessToken()->getAccessToken(),
                'Content-type' => 'application/json'
            ],
            json_encode($searchBody)
        ));

        if ($response->getStatusCode() !== 200) {
            throw new ValidooApiException('Bad search response; ' . $response->getReasonPhrase(), $response->getStatusCode());
        }

        return array_map(function(array $product) {
            return Product::fromArray($product);
        }, json_decode((string)$response->getBody(), true)['results']);
    }

    /**
     * Get a product by $itemId, will include CIN data in the requested format
     * @param string $itemId
     * @param bool $includeParentData
     * @param string $formatXml
     * @param string $dataType
     * @return Product
     */
    public function getProduct(
        string $itemId,
        bool $includeParentData = true,
        string $formatXml = CinFormat::XML,
        string $dataType = DataType::PRODUCT): Product
    {
        $response = $this->client->request(
            'GET',
            'TradeItemInformation/getItemById',
            [
                'headers' => [
                    'Authorization' => $this->getAccessToken()->getTokenType() . ' ' . $this->getAccessToken()->getAccessToken(),
                    'Content-type' => 'application/json'
                ],
                'query' => [
                    'id' => $itemId,
                    'cinFormat' => $formatXml,
                    'dataType' => $dataType
                ]
            ]
        );

        if ($response->getStatusCode() !== 200) {
            throw new ValidooApiException('Bad getProduct response; ' . $response->getReasonPhrase(), $response->getStatusCode());
        }

        $product = Product::fromArray(json_decode((string)$response->getBody(), true)[0]);

        if ($includeParentData) {
            //A product might have many layers of cases, i.e. "Bottle > 6pack > Case of 6packs > Pallet"
            $caseLevel = 0;

            foreach ($this->getParentsOfItem($product->getGtin(), DataType::GDSN) as $parent) {
                if ($product->getSupplierArticleNumber() === "" && $parent->getSupplierArticleNumber() !== "") {
                    $product->setSupplierArticleNumber($parent->getSupplierArticleNumber());
                }
                if ($parent->getCin()->getUnitDescriptorCode() === UnitDescriptorCode::CASE) {
                    $product->getCin()->add(strtolower(UnitDescriptorCode::CASE) . '.' . $caseLevel, $parent->getCin()->jsonSerialize());
                    $caseLevel++;
                } else {
                    $product->getCin()->add(strtolower($parent->getCin()->getUnitDescriptorCode()), $parent->getCin()->jsonSerialize());
                }
            }
        }

        return $product;
    }

    /**
     * @return Token
     */
    public function getAccessToken(): Token
    {
        if (isset($this->accessToken) && $this->accessToken->getExpires() > new \DateTime()) {
            return $this->accessToken;
        }
        return $this->acquireToken();
    }

    /**
     * @param Token $accessToken
     * @return ValidooApiWrapper
     */
    public function setAccessToken(Token $accessToken): ValidooApiWrapper
    {
        $this->accessToken = $accessToken;
        return $this;
    }

    /**
     * @param string $gtin
     * @param string $dataType
     * @return Product[]
     * @throws ValidooApiException
     */
    protected function getParentsOfItem(string $gtin, string $dataType = DataType::PRODUCT): array
    {
        try {
            $response = $this->client->request(
                'GET',
                'TradeItemInformation/parentOfTradeItem',
                [
                    'headers' => [
                        'Authorization' => $this->getAccessToken()->getTokenType() . ' ' . $this->getAccessToken()->getAccessToken(),
                        'Content-type' => 'application/json'
                    ],
                    'query' => [
                        'gtin' => $gtin,
                        'dataType' => $dataType
                    ]
                ]
            );

            if ($response->getStatusCode() !== 200) {
                throw new ValidooApiException('Bad getParentOfItem response; ' . $response->getReasonPhrase(), $response->getStatusCode());
            }

            $responseBody = json_decode((string)$response->getBody(), true);

            if (empty($responseBody['results'])) {
                throw new ValidooApiException('Parent not found');
            }
        } catch (ClientException $ce) {
            if ($ce->getCode() === 400) {
                throw new ValidooApiException('Parent not found');
            }
            throw $ce;
        }

        $parent = $responseBody['results'][0];
        $parentProducts = [];
        while ($parent) {
            $parentProducts[] = $this->getItemByIdentification($parent, CinFormat::XML, $dataType);

            if (!empty($parent['childTradeItems'][0])) {
                $parent = $parent['childTradeItems'][0];
            } else {
                $parent = null;
            }
        }

        return $parentProducts;
    }

    protected function getItemByIdentification(array $parentData, string $formatXml = CinFormat::XML, string $dataType = DataType::PRODUCT): Product
    {
        $response = $this->client->request(
            'GET',
            'TradeItemInformation/getItemByIdentification',
            [
                'headers' => [
                    'Authorization' => $this->getAccessToken()->getTokenType() . ' ' . $this->getAccessToken()->getAccessToken(),
                    'Content-type' => 'application/json'
                ],
                'query' => [
                    'gtin' => $parentData['gtin'],
                    'informationProviderGln' => $parentData['informationProviderGln'],
                    'targetMarket' => $parentData['targetMarketCountryCode'],
                    'cinFormat' => $formatXml,
                    'dataType' => $dataType
                ]
            ]
        );

        if ($response->getStatusCode() !== 200) {
            throw new ValidooApiException('Bad getItemByIdentification response; ' . $response->getReasonPhrase(), $response->getStatusCode());
        }

        return Product::fromArray(json_decode((string)$response->getBody(), true)[0])
            //The "is despatch unit" is not found in the product response, only in the "parentOfTradeItem" response (possibly in the CIN data as well)
            ->setIsTradeItemADespatchUnit($parentData['isTradeItemADespatchUnit'] ?? false);
    }
}