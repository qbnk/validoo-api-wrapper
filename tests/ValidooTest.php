<?php

namespace QBNK\JobQueue\Job\Test;

use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use QBNK\Integration\Validoo\Model\CatalogueItemNotification;
use QBNK\Integration\Validoo\Model\Credentials;
use QBNK\Integration\Validoo\Model\Enum\Scope;
use QBNK\Integration\Validoo\Model\ItemQaStatus;
use QBNK\Integration\Validoo\Model\Product;
use QBNK\Integration\Validoo\Model\Search;
use QBNK\Integration\Validoo\Model\Token;
use QBNK\Integration\Validoo\ValidooApiWrapper;

class ValidooTest extends TestCase
{
    public function testCinDecode()
    {
        $cin = CatalogueItemNotification::fromPayload($this->getCinData());

        $this->assertCount(22, $cin->flatten());
        $this->assertEquals('1163 kJ', $cin->get('nutritional.energy')[0]);

        $jsonReconstructed = CatalogueItemNotification::fromArray($cin->jsonSerialize());
        $this->assertCount(count($cin->flatten()), $jsonReconstructed->flatten());
        $this->assertEquals($cin->get('nutritional.energy')[0], $jsonReconstructed->get('nutritional.energy')[0]);
    }

    public function testToken()
    {
        $data = [
            'accessToken' => 'foo',
            'refreshToken' => 'bar',
            'expires' => new \DateTime('2022-03-12'),
            'tokenType' => 'Bearer',
            'scope' => [Scope::DIGITAL_ASSETS, Scope::OFFLINE_ACCESS]
        ];

        $token = Token::fromArray($data);

        foreach ($data as $getter => $value) {
            $this->assertEquals($value, $token->{'get' . ucfirst($getter)}());
        }

        $jsonSerialiased = $token->jsonSerialize();
        foreach (['accessToken', 'refreshToken', 'tokenType'] as $param) {
            $this->assertEquals($data[$param], $jsonSerialiased[$param]);
        }
        $this->assertEquals($data['expires']->format(DATE_ATOM), $jsonSerialiased['expires']);
        $this->assertEquals(implode(',', $data['scope']), $jsonSerialiased['scope']);
    }

    public function testCredentials()
    {
        $data = $this->getDummyCredentials();

        $credentials = Credentials::fromArray($data);

        foreach ($data as $getter => $value) {
            $this->assertEquals($value, $credentials->{'get' . ucfirst($getter)}());
        }

        $jsonSerialiased = $credentials->jsonSerialize();
        foreach (['clientId', 'clientSecret', 'username', 'password'] as $param) {
            $this->assertEquals($data[$param], $jsonSerialiased[$param]);
        }
        $this->assertEquals(implode(',', $data['scope']), $jsonSerialiased['scope']);
    }

    public function testAcquireToken()
    {
        $freshToken = $this->getDummyToken(1);

        $requestHistory = [];
        $historyMiddleware = Middleware::history($requestHistory);
        $validoo = $this->getMockValidooApi(
            [
                new Response(200, [], json_encode($freshToken)),
                $this->getTokenResponse(),
            ],
            $historyMiddleware
        );

        //Should acquire a new token
        $token = $validoo->getAccessToken();

        $this->assertEquals($freshToken['access_token'], $token->getAccessToken());
        $this->assertCount(1, $requestHistory);
        /** @var Request $request */
        $request = $requestHistory[0]['request'];
        $this->assertEquals('https://identity.validoo.se/connect/token', (string)$request->getUri());

        sleep(1);

        //Should acquire a new token since the previous one will have expired
        $tokenTwo = $validoo->getAccessToken();
        $this->assertCount(2, $requestHistory);
        $this->assertNotEquals($token->getAccessToken(), $tokenTwo->getAccessToken());

        sleep(1);

        //Should return the same token since it's still active
        $tokenThree = $validoo->getAccessToken();
        $this->assertCount(2, $requestHistory);
        $this->assertEquals($tokenThree->getAccessToken(), $tokenTwo->getAccessToken());
    }

    /**
     * @return void
     */
    public function testItemQaStatus()
    {
        $parameters = $this->getDummyQaStatus();

        $qaStatus = ItemQaStatus::fromArray($parameters);
        $qaStatusJson = (array)$qaStatus->jsonSerialize();

        $assertions = 0;
        foreach ($parameters as $parameter => $value) {
            $modelValue = $qaStatus->{"get{$parameter}"}();
            $this->assertEquals(
                $value,
                $modelValue instanceof \DateTime ? $modelValue->format(Product::DATE_FORMAT) : $modelValue,
                "qaStatus->get$parameter did not return expected \"$value\""
            );
            $this->assertEquals($value, $qaStatusJson[$parameter]);
            $assertions++;
        }

        $this->assertEquals(count($parameters), $assertions, 'Failed to perform all assertions');

    }

    /**
     * @depends testItemQaStatus
     * @return void
     */
    public function testProduct()
    {
        $parameters = $this->getTestProductParameters();

        $product = Product::fromArray($parameters);

        $assertions = 2;
        foreach ($parameters as $parameter => $value) {
            if ($parameter !== 'qaStatus' && $parameter !== 'cin') {
                $modelValue = $product->{"get{$parameter}"}();
                $this->assertEquals(
                    $value,
                    $modelValue instanceof \DateTime ? $modelValue->format(Product::DATE_FORMAT) : $modelValue,
                    "product->get$parameter did not return expected \"$value\""
                );
                $assertions++;
            }
        }

        $this->assertEquals(count($parameters), $assertions, 'Failed to perform all assertions');

        $this->assertInstanceOf(ItemQaStatus::class, $product->getQaStatus());
        $this->assertEquals($parameters['qaStatus']['digitalQAStatus'], $product->getQaStatus()->getDigitalQAStatus());
        $this->assertInstanceOf(CatalogueItemNotification::class, $product->getCin());
        $this->assertEquals('1163 kJ', $product->getCin()->get('nutritional.energy.0'));

        $productReconstructed = Product::fromArray(json_decode(json_encode($product->jsonSerialize()), true));

        $this->assertInstanceOf(ItemQaStatus::class, $productReconstructed->getQaStatus());
        $this->assertEquals($parameters['qaStatus']['digitalQAStatus'], $productReconstructed->getQaStatus()->getDigitalQAStatus());
        $this->assertInstanceOf(CatalogueItemNotification::class, $productReconstructed->getCin());
        $this->assertEquals('1163 kJ', $productReconstructed->getCin()->get('nutritional.energy.0'));
    }

    /**
     * @depends testProduct
     * @return void
     */
    public function testSearchModel()
    {
        $gtins = ['foo', 'bar'];
        $changeFrom = new \DateTime('2000-01-01 10:00');
        $changeTo = new \DateTime('2001-01-01 13:37');

        $searchModelArray = (new Search())
            ->setGtins($gtins)
            ->setLastChangeFrom($changeFrom)
            ->setLastChangeTo($changeTo)
            ->jsonSerialize()
        ;

        $this->assertEqualsCanonicalizing($gtins, $searchModelArray['gtins']);
        $this->assertEquals($changeFrom->format(DATE_ATOM), $searchModelArray['lastChangeDateTimeFrom']);
        $this->assertEquals($changeTo->format(DATE_ATOM), $searchModelArray['lastChangeDateTimeTo']);
    }

    /**
     * @depends testSearchModel
     * @return void
     */
    public function testSearch()
    {
        $parameters = $this->getTestProductParameters();
        $expectedResponse = new Response(200, [], json_encode([
            'results' => [
                $parameters
            ]
        ]));

        $validooApiWrapper = $this->getMockValidooApi([$this->getTokenResponse(), $expectedResponse]);

        $response = $validooApiWrapper->search(new Search());

        $this->assertCount(1, $response);
        $this->assertInstanceOf(Product::class, $response[0]);

        $expectedProduct = Product::fromArray($parameters);
        $this->assertEquals($expectedProduct->getGtin(), $response[0]->getGtin());
        $this->assertEquals($expectedProduct->getItemId(), $response[0]->getItemId());
    }

    /**
     * @depends testProduct
     * @return void
     */
    public function testGetProduct()
    {
        $parameters = $this->getTestProductParameters();
        $expectedResponse = new Response(200, [], json_encode([$parameters]));

        $validooApiWrapper = $this->getMockValidooApi([
            $this->getTokenResponse(),
            $expectedResponse,
            new Response(200, [], file_get_contents(__DIR__ . '/parent_response.json')),
            new Response(200, [], file_get_contents(__DIR__ . '/pallet_response.json')),
            new Response(200, [], file_get_contents(__DIR__ . '/case_response.json'))
        ]);

        $expectedProduct = Product::fromArray($parameters);
        $response = $validooApiWrapper->getProduct($expectedProduct->getItemId());

        $this->assertInstanceOf(Product::class, $response);
        $this->assertEquals($expectedProduct->getGtin(), $response->getGtin());
        $this->assertEquals($expectedProduct->getItemId(), $response->getItemId());
        $cinData = $response->getCin();
        $this->assertInstanceOf(CatalogueItemNotification::class, $cinData);
        $this->assertArrayHasKey('pallet.containerInfo.totalQuantityOfNextLowerLevelTradeItem', $cinData);
        $this->assertArrayHasKey('case.0.containerInfo.totalQuantityOfNextLowerLevelTradeItem', $cinData);
        $this->assertEquals(CatalogueItemNotification::VALUE_NOT_AVAILABLE, $cinData->get('case.0.completeLayers'));
        $this->assertEquals('9', $cinData->get('pallet.completeLayers'));
    }

    private function getMockValidooApi(array $expectedResponses, callable $historyMiddleware = null): ValidooApiWrapper
    {
        $mockHandler = HandlerStack::create(new MockHandler($expectedResponses));
        if ($historyMiddleware) {
            $mockHandler->push($historyMiddleware);
        }
        return (new ValidooApiWrapper(
            Credentials::fromArray($this->getDummyCredentials()),
            [
                'handler' => $mockHandler
            ]
        ));
    }

    private function getTestProductParameters()
    {
        return [
            'gtin' => '00000040413198',
            'cin' => $this->getCinData(),
            'informationProviderGln' => '7320240030135',
            'informationProviderName' => 'The Whole Company',
            'targetMarketCountryCode' => '752',
            'targetMarketName' => 'Sweden',
            'brandOwnerGln' => '7320240030135',
            'brandOwnerName' => 'The Whole Company',
            'descriptiveSize' => '7 st',
            'brandName' => 'The Whole Brand',
            'gpcCode' => '10000206',
            'isTradeItemABaseUnit' => true,
            'isTradeItemAConsumerUnit' => true,
            'isTradeItemAnOrderableUnit' => true,
            'isTradeItemADisplayUnit' => true,
            'isTradeItemADespatchUnit' => true,
            'creationDateTime' => '2019-07-05T13:25:29.017Z',
            'effectiveDateTime' => '2019-07-05T13:25:29.017Z',
            'startAvailabilityDate' => '2019-07-05T13:25:29.017Z',
            'consumerFirstAvailabilityDateTime' => '2019-07-05T13:25:29.017Z',
            'lastChangeDateTime' => '2019-07-05T13:25:29.017Z',
            'firstShipDateTime' => '2019-07-05T13:25:29.017Z',
            'functionalName' => 'Whole Organic',
            'qaStatus' => $this->getDummyQaStatus(),
            'itemId' => 12345,
            'status' => 'Published',
            'topItem' => true
        ];
    }

    private function getDummyToken(int $expiresIn): array
    {
        return [
            'access_token' => uniqid('', true),
            'expires_in' => $expiresIn,
            'token_type' => 'Bearer',
            'refresh_token' => uniqid('', true),
            'scope' => Scope::OFFLINE_ACCESS . ' ' . Scope::TRADE_ITEM
        ];
    }

    private function getCinData(): string
    {
        return file_get_contents(__DIR__ . '/example.cin');
    }

    private function getDummyCredentials(): array
    {
        return [
            'clientId' => 'foo',
            'clientSecret' => 'bar',
            'username' => 'baz',
            'password' => '1234',
            'scope' => [Scope::DIGITAL_ASSETS, Scope::OFFLINE_ACCESS]
        ];
    }

    private function getTokenResponse(): Response
    {
        return new Response(200, [], json_encode($this->getDummyToken(3600)));
    }

    private function getDummyQaStatus(): array
    {
        return [
            'digitalQAStatus' => 'OK',
            'digitalQAStatusChangeDateTime' => '2019-07-05T13:25:29.017Z',
            'measurementQAStatus' => 'OK',
            'measurementQAStatusChangeDateTime' => '2019-07-05T13:25:29.017Z',
            'barcodeQAStatus' => 'OK',
            'barcodeQAStatusChangeDateTime' => '2019-07-05T13:25:29.017Z',
            'itemQAResultStatusChangeDateTime' => '2019-07-05T13:25:29.017Z'
        ];
    }
}
